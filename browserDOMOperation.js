/*!
browserDOMOperation.js
date 1738737305040 [Wed Feb 05 2025 14:35:05 GMT+0800 (China Standard Time)]
(c) 2025 Bright_Leader
Licensed under the Apache-2.0.
*/
(function exportFunction(This,factory){"use strict";
/*
written in ECMAScript 5 specification, with ECMAScript 6 keywords "let" and "const"
- This script is meant to be ECMAScript 3-compliant.
- When all "let" and "const" keywords are replaced with keyword "var",
- the script is ECMAScript 3-compliant.

----

- Standard ECMA-262
http://www.ecma-international.org/publications-and-standards/standards/ecma-262/

- ECMA-262, 3rd edition, December 1999
http://www.ecma-international.org/wp-content/uploads/ECMA-262_3rd_edition_december_1999.pdf

- ECMA-262, 5th edition, December 2009
http://www.ecma-international.org/wp-content/uploads/ECMA-262_5th_edition_december_2009.pdf

- ECMA-262, 5.1th edition, June 2011
http://262.ecma-international.org/5.1/index.html
http://www.ecma-international.org/wp-content/uploads/ECMA-262_5.1_edition_june_2011.pdf

- ECMA-262, 6th edition, June 2015
http://262.ecma-international.org/6.0/index.html
http://www.ecma-international.org/wp-content/uploads/ECMA-262_6th_edition_june_2015.pdf
*/
const typeFunction="function";

const isObjectType=function(value){

let typeOfValue;

  return !!value&&(
    (typeOfValue=typeof value)==="object"||
    typeOfValue===typeFunction
  );
};

const returnOrThrowObjectType=function(){

function valueIsPrimitiveError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not an object type (an object or a function)!"
  );
}

  return function(value){
    if(isObjectType(value))return value;
    else throw valueIsPrimitiveError(value);
  };
}();

const returnOrThrowFunction=function(){

function notAFunctionError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a function!"
  );
}

  return function(value){
    if(typeof value===typeFunction)return value;
    else throw notAFunctionError(value);
  };
}();

const False=false,
  True=true;

const constructor_key="constructor",
  prototype_key="prototype";

const Function_constructor=returnOrThrowObjectType(exportFunction[constructor_key]),
  Object_constructor=returnOrThrowFunction({}[constructor_key]),
  Array_constructor=returnOrThrowFunction([][constructor_key]);

const Function_prototype=returnOrThrowObjectType(Function_constructor[prototype_key]),
  Object_prototype=returnOrThrowObjectType(Object_constructor[prototype_key]);

const call_key="call";

const Function_call=returnOrThrowFunction(Function_prototype[call_key]),
  Object_hasOwnProperty=returnOrThrowFunction(Object_prototype.hasOwnProperty);

const relinquishFunction=function(item,key,target){

returnOrThrowObjectType(target);
let targetOriginallyHasOwnTargetProperty,targetOriginalTargetPropertyValue;
targetOriginallyHasOwnTargetProperty=Function_call[call_key](Object_hasOwnProperty,target,key);
targetOriginalTargetPropertyValue=target[key];

  return function(){

if(target[key]===item){ /* relinquishes the target property only when its value is the current item */

(
  !targetOriginallyHasOwnTargetProperty&&
  Function_call[call_key](Object_hasOwnProperty,target,key)
)?(delete target[key]):(target[key]=targetOriginalTargetPropertyValue);
/* re-evaluates these variables */
targetOriginallyHasOwnTargetProperty=Function_call[call_key](Object_hasOwnProperty,target,key);
(
  key in target&&
  target[key]!==item&& /* whether the property value is successfully changed */
  (targetOriginalTargetPropertyValue=target[key])
);

}

    return item;
  };
};

const tryES5CreateNullPrototypeObject=function(){

const create=Object_constructor.create;
const createIsFunction=typeof create===typeFunction;

  return function(){

let temp0;

    return isObjectType(temp0=
      createIsFunction&&
      create(null)
    )?temp0:new Object_constructor();
  };
}();

const arrayWithOneItem=function(item){

const newArray=new Array_constructor(1);
newArray[0]=item;

  return newArray;
};

const name="browserDOMOperation";

const source_key="source",
  isAsynchronous_key="isAsynchronous";

let var0,
  isAsynchronous;

isAsynchronous=False;

/* eslint {
     "no-undef": [
       "error",
       {
         "typeof": true
       }
     ]
   }
--
ESLint [ http://eslint.org/ ] configurations
*/

(
  (typeof
    define /* eslint-disable-line no-undef */
  ===typeFunction)&&
  typeof (var0=
    define /* eslint-disable-line no-undef */
  )===typeFunction&&
  (typeof
    requirejs /* eslint-disable-line no-undef */
  ===typeFunction)&&
  isObjectType(var0.amd)&& /* RequireJS included? [ http://requirejs.org/ ] (based on version 2.0.4 [ http://requirejs.org/docs/release/2.0.4/comments/require.js ] ) */
  var0(
    arrayWithOneItem("exports"),
    function(exports){
      returnOrThrowObjectType(exports)[source_key]="RequireJS [ http://requirejs.org/ ]";
      exports[isAsynchronous_key]=isAsynchronous;
      return factory(exports,This);
    }
  )
);

(
  isObjectType(This)&&
  isObjectType(This.document)&&
  (
    This.frames===This||
    This.self===This||
    This.window===This
  )&& /* browser environment */
  (This[name]=function(){

const product=tryES5CreateNullPrototypeObject();
product.relinquish=relinquishFunction(product,name,This);
product[source_key]="this";
product[isAsynchronous_key]=isAsynchronous;

    return factory(product,This);
  }())
);

  isAsynchronous=True;
  return;
})(
  this,
  function factoryFunction(objectToExport,bwindow){"use strict";
/* @license
Copyright 2025 Bright_Leader

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function isPrimitive(value){

let typeOfValue;

  return !value||(
    (typeOfValue=typeof value)!=="object"&&
    typeOfValue!=="function"
  );
}

const returnOrThrowObjectType=function(){

function valueIsPrimitiveError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not an object type (an object or a function)!"
  );
}

  return function(value){
    if(isPrimitive(value))throw valueIsPrimitiveError(value);
    else return value;
  };
}();

returnOrThrowObjectType(objectToExport);

function assertEager(condition,message){
  if(condition)return condition;
  else throw 1 in arguments?message:"Assertion failed!";
}

assertEager(
  returnOrThrowObjectType(bwindow).window===bwindow,
  "This script may not currently be in browser environment!"
);

function isFunction(value){
  return typeof value==="function";
}

const returnOrThrowFunction=function(){

function notAFunctionError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a function!"
  );
}

  return function(value){
    if(isFunction(value))return value;
    else throw notAFunctionError(value);
  };
}();

const returnOrThrow_makeFunction=function(checkFunction,errorFunction){

returnOrThrowFunction(checkFunction);
returnOrThrowFunction(errorFunction);

  return function(value){
    if(checkFunction(value))return value;
    else throw errorFunction(value);
  };
};

function isObjectType(value){

let typeOfValue;

  return !!value&&(
    (typeOfValue=typeof value)==="object"||
    typeOfValue==="function"
  );
}

function isNullish(value){
  return value===void null||value===null;
}

function isNotNullish(value){
  return value!==null&&value!==void null;
}

const SIGNED_INT32_MAX=2147483647,
  SIGNED_INT32_MIN=-2147483648;

function isNumber(value){
  return typeof value==="number";
}

const isInteger=function(value){
  return isNumber(value)&&(
    value<=SIGNED_INT32_MAX&&value>=SIGNED_INT32_MIN?(value^0)===value:value%1===0
  );
};

const infinity=1/0,
  negativeInfinity=-1/0;

const isNotInfinity=function(value){
  return value!==infinity&&value!==negativeInfinity;
};

function isNotNaN(value){
  return value===value;
}

const isFiniteNumber=function(value){
  return isNumber(value)&&isNotNaN(value)&&isNotInfinity(value);
};

const forceToInteger=function(value){
  return (
    isFiniteNumber(value)&&
    (
      value>SIGNED_INT32_MAX||
      value<SIGNED_INT32_MIN
    )
  )?value-(value%1):value^0;
};

const UNSIGNED_INT32_MAX=4294967295;

const isUint32=function(value){
  return isInteger(value)&&value>=0&&value<=UNSIGNED_INT32_MAX;
};

const UNSIGNED_INT16_MAX=65535;

const isUint16=function(value){
  return (value&UNSIGNED_INT16_MAX)===value;
};

function minTwo(valueA,valueB){
  return valueA<valueB?valueA:valueB;
}

function maxTwo(valueA,valueB){
  return valueA>valueB?valueA:valueB;
}

const roundInteger=function(left,right,num){

const leftInt=forceToInteger(left),
  rightInt=forceToInteger(right);
const leftEdge=minTwo(leftInt,rightInt);
const rightEdge=maxTwo(leftInt,rightInt);
const toBeRanged=forceToInteger(num);
const rangeWidth=rightEdge-leftEdge+1;

  return forceToInteger(
    leftEdge!==rightEdge?(
      toBeRanged>rightEdge?(
        (toBeRanged-leftEdge)%rangeWidth+leftEdge
      ):(
        toBeRanged<leftEdge?(
          rightEdge-(rightEdge-toBeRanged)%rangeWidth
        ):toBeRanged
      )
    ):leftEdge
  );
};

const mathPow2_53minus1=9007199254740991;

const toLength=function(num){

const len=forceToInteger(num);
if(len<=0)return 0;
if(len===infinity)return mathPow2_53minus1;

  return minTwo(len,mathPow2_53minus1);
};

const toUint32=function(num){
  return roundInteger(0,UNSIGNED_INT32_MAX,num);
};

/* ECMAScript native item(s) */

const constructor_key="constructor",
  prototype_key="prototype",
  call_key="call";

const False=false,
  True=true;

const Function_constructor=returnOrThrowFunction(factoryFunction[constructor_key]),
  Object_constructor=returnOrThrowFunction({}[constructor_key]);
const Function_prototype=returnOrThrowObjectType(Function_constructor[prototype_key]),
  Object_prototype=returnOrThrowObjectType(Object_constructor[prototype_key]);
const Function_call=returnOrThrowFunction(Function_prototype[call_key]),
  Function_apply=returnOrThrowFunction(Function_prototype.apply),
  Object_hasOwnProperty=returnOrThrowFunction(Object_prototype.hasOwnProperty);

const Array_constructor=returnOrThrowFunction([][constructor_key]);

const Array_prototype=returnOrThrowObjectType(Array_constructor[prototype_key]);

const isArray=function(value){
  return isObjectType(value)&&value[constructor_key]===Array_constructor;
};

const String_constructor=returnOrThrowObjectType(new Object_constructor("")[constructor_key]),
  RegExp_constructor=returnOrThrowFunction(/(?:)/[constructor_key]);

const String_prototype=returnOrThrowObjectType(String_constructor[prototype_key]);

(function(){ /* module: tests whether the browser is under Content-Security-Policy restrictions */

objectToExport.cspUnsafeEval=function(){

const String_match=returnOrThrowFunction(String_prototype.match),
  Object_toString=returnOrThrowFunction(Object_prototype.toString);

  return function(){ /* "script-src" CSP directive test */

let tempFunction;

try{

tempFunction=new Function_constructor("x,y","return x|y;");

      return !(
        isFunction(tempFunction)&&
        tempFunction(1,2)===3
      );

}catch(err){

      return(
        isNullish(tempFunction)&&
        isObjectType(err)&&
        (
          isArray(
            Function_call[call_key](
              String_match,
              err,
              new RegExp_constructor("unsafe-eval|CSP")
            )
          )||
          Function_call[call_key](
            Object_toString,
            err
          )==="[object Error]"
        )
      );

}

  };
}();

  return;
})();

(function(){ /* module: a function that filters out XML entities from a string */

objectToExport.xmlInnerText=function(){

const String_replace=returnOrThrowFunction(String_prototype.replace);

function xmlEntitiesFilterFn(match){
  switch(match){
    case "\x3C":return "\x26#60;";
    case "\x26":return "\x26#38;";
    case "\x3E":return "\x26#62;";
    case "\x27":return "\x26#39;";
    case "\x22":return "\x26#34;";
  }
  return match;
}

  return function(str){
    return isNotNullish(str)?Function_call[call_key](
      String_replace,
      str, /*
Passing an empty value (undefined or null) here will result in
"TypeError: can't convert undefined to object"
or
"TypeError: can't convert null to object",
regardless of whether it is in strict mode.
The reason behind this is here: [ http://262.ecma-international.org/5.1/#sec-9.10 ].
*/
      new RegExp_constructor("\\x3C|\\x26|\\x3E|\\x27|\\x22","g"),
      xmlEntitiesFilterFn
    ):"";
  };
}();

  return;
})();

const isPlainObject=function(value){

let temp0;

  return(
    isObjectType(value)&&
    (
      value[temp0=constructor_key]===Object_constructor||
      !(temp0 in value) /*
In ECMAScript 5 [ http://www.ecma-international.org/wp-content/uploads/ECMA-262_5th_edition_december_2009.pdf ],
objects created with "Object.create(null)" has no constructor.
This function takes that case into consideration,
despite its rarity.
*/
    )
  );
};

const length_key="length";

const isArrayLike=function(value){

let temp0;

  return(
    isObjectType(value)&&
    (
      (temp0=value[constructor_key])===Array_constructor||
      (
        temp0!==Function_constructor&& /* Functions have "length" property but should not be considered as array-like objects, which is a common trap. */
        (temp0=length_key) in value&&
        isUint32(value[temp0])
      )
    )
  );
};

const tryES5CreateNullPrototypeObject=function(){

const create=Object_constructor.create;
const createIsFunction=isFunction(create);

  return function(){

let temp0;

    return isObjectType(temp0=
      createIsFunction&&
      create(null)
    )?temp0:new Object_constructor();
  };
}();

const tryES5DefineProperty=function(){

const defineProperty=Object_constructor.defineProperty;
const definePropertyIsFunction=isFunction(defineProperty);

  return function(target,propertyName,propertyValue,isWritable,isEnumerable,isConfigurable){

returnOrThrowObjectType(target);
let tempObject;
if(definePropertyIsFunction){
  (tempObject=tryES5CreateNullPrototypeObject()).value=propertyValue;
  tempObject.writable=isWritable;
  tempObject.enumerable=isEnumerable;
  tempObject.configurable=isConfigurable;
  defineProperty(target,propertyName,tempObject);
}else target[propertyName]=propertyValue;

    return target;
  };
}();

const createNullPrototypeArrayLikeObject=function(){

const newObject=tryES5CreateNullPrototypeObject();
const args=arguments;
const len=args[length_key];
tryES5DefineProperty(
  newObject,

  length_key,len,

  True,False,False
);
let i_var;
for(i_var=0;i_var<len;i_var+=1)tryES5DefineProperty(
  newObject,

  i_var,
  args[i_var],

  True,True,True
);

  return newObject;
};

const assignMultipleProperties=function(target,keys,values,writable,enumerable,configurable){

returnOrThrowObjectType(target);
returnOrThrowObjectType(keys);
returnOrThrowObjectType(values);
let i_var,len;
for(
  i_var=0,
  len=toLength(keys[length_key]);
  i_var<len;
  i_var+=1
)(
  Function_call[call_key](Object_hasOwnProperty,keys,i_var)&&
  Function_call[call_key](Object_hasOwnProperty,values,i_var)&&
  tryES5DefineProperty(target,keys[i_var],values[i_var],writable,enumerable,configurable)
);

  return target;
};

const valueOf_key="valueOf";

const Object_valueOf=returnOrThrowFunction(Object_prototype[valueOf_key]);

const toObject=function(value){
  return returnOrThrowObjectType(
    Function_call[call_key](Object_valueOf,value)
  );
};

/* some array-like methods */

const arrayLikeFindIndex=function(predicate,predicateThisArg){

returnOrThrowFunction(predicate);
const self=toObject(this);
const len=toLength(self[length_key]);
const predicateThisArgIsNullish=isNullish(predicateThisArg);
let key,value;

for(key=0;key<len;key+=1){

value=self[key];
if(predicateThisArgIsNullish?predicate(value,key,self):Function_call[call_key](predicate,predicateThisArg,value,key,self))
  return key;

}

  return -1;
};

const arrayLikeFilter=function(callbackFn,callbackFnThisArg){

returnOrThrowFunction(callbackFn);
const self=toObject(this);
const callbackFnThisArgIsNullish=isNullish(callbackFnThisArg);
const len=toUint32(self[length_key]);
let k_var,t_var,v_var;
k_var=t_var=0;
const newArray=new Array_constructor();
while(k_var<len){

if(k_var in self){

v_var=self[k_var];

if(callbackFnThisArgIsNullish?callbackFn(v_var,k_var,self):Function_call[call_key](callbackFn,callbackFnThisArg,v_var,k_var,self)){

tryES5DefineProperty(
  newArray,

  t_var,v_var,

  True,True,True
);
t_var+=1;

}

}

k_var+=1;

}

  return newArray;
};

const arrayLikeMap=function(callbackFn,callbackFnThisArg){

returnOrThrowFunction(callbackFn);
const self=toObject(this);
const len=toUint32(self[length_key]);
const newArray=new Array_constructor(len);
const callbackFnThisArgIsNullish=isNullish(callbackFnThisArg);
let key,value;
for(key=0;key<len;key+=1){

if(key in self){

value=self[key];
tryES5DefineProperty(
  newArray,

  key,
  callbackFnThisArgIsNullish?callbackFn(value,key,self):Function_call[call_key](callbackFn,callbackFnThisArg,value,key,self),

  True,True,True
);

}
  
}

  return newArray;
};

const arrayLikeForEach=function(callbackFn,callbackFnThisArg){

returnOrThrowFunction(callbackFn);
const self=toObject(this);
let key,value;
const callbackFnThisArgIsNullish=isNullish(callbackFnThisArg);
const len=toUint32(self[length_key]);
for(key=0;key<len;key+=1){

if(key in self){

value=self[key];
callbackFnThisArgIsNullish?callbackFn(value,key,self):Function_call[call_key](callbackFn,callbackFnThisArg,value,key,self);

}

}

  return self;
};

const arrayLikeIndexOf=function(searchElement,fromIndex){

const self=toObject(this);
const len=toUint32(self[length_key]);
if(len===0)return -1;
const n_var=isNullish(fromIndex)?0:forceToInteger(fromIndex);
if(n_var>=len)return -1;
let k_var;
if(n_var>=0)k_var=n_var;
else{

k_var=len+n_var;
(k_var<0)&&(k_var=0);

}

while(k_var<len){

if(k_var in self){

if(self[k_var]===searchElement)return k_var;

}

k_var+=1;

}

  return -1;
};

const arrayLikeEvery=function(callbackFn,callbackFnThisArg){

returnOrThrowFunction(callbackFn);
const self=toObject(this);
const len=toUint32(self[length_key]);
const callbackFnThisArgIsNullish=isNullish(callbackFnThisArg);
let k_var,v_var;
for(k_var=0;k_var<len;k_var+=1){

if(k_var in self){

v_var=self[k_var];
if(!(callbackFnThisArgIsNullish?callbackFn(v_var,k_var,self):Function_call[call_key](callbackFn,callbackFnThisArg,v_var,k_var,self)))
  return False;

}

}

  return True;
};

const arrayLikeSome=function(callbackFn,callbackFnThisArg){

returnOrThrowFunction(callbackFn);
const self=toObject(this);
const len=toUint32(self[length_key]);
const callbackFnThisArgIsNullish=isNullish(callbackFnThisArg);
let key,value;
for(key=0;key<len;key+=1){

if(key in self){

value=self[key];
if(callbackFnThisArgIsNullish?callbackFn(value,key,self):Function_call[call_key](callbackFn,callbackFnThisArg,value,key,self))
  return True;

}
  

}

  return False;
};

const arrayLikeDeduplicate=function(){

const assist=function(value,key,array){
  return Function_call[call_key](arrayLikeIndexOf,array,value)===key;
};

  return function(arrayLike){
    return Function_call[call_key](arrayLikeFilter,arrayLike,assist);
  };
}();

const undef=void null;

const defineDontEnumProperty=function(target,key,value){
  return tryES5DefineProperty(target,key,value,True,False,True);
};

let temp0,temp1,temp2;

const IteratorResult=function(){

const value_key="value",
  done_key="done";

temp0=function(value,done){

const self=returnOrThrowObjectType(this);
tryES5DefineProperty(
  self,

  value_key,
  value,

  True,True,True
);
tryES5DefineProperty(
  self,

  done_key,
  done?True:False,

  True,True,True
);

  return self;
};

temp1=temp0[prototype_key];

defineDontEnumProperty(
  temp1,
  value_key,undef
);

defineDontEnumProperty(
  temp1,
  done_key,False
);

  return temp0;
}();

/* native symbol support */

const nSymbol=bwindow.Symbol;

function isSymbol(value){
  return typeof value==="symbol";
}

const iterator_key="iterator";

const nSymbolIsFunction=(
  isFunction(nSymbol)&&
  isSymbol(nSymbol[iterator_key])
);

const retrieveWellKnownSymbol=function(key){
  let tempKey;
  return(
    nSymbolIsFunction&&
    key in nSymbol&&
    isSymbol(tempKey=nSymbol[key])
  )?tempKey:"@@"+key;
};

function identity(value){
  return value;
}

const createUniqueSymbol=nSymbolIsFunction?nSymbol:identity;

const toStringTag_key="toStringTag",
  isConcatSpreadable_key="isConcatSpreadable";

const iterator_wellKnownSymbol=retrieveWellKnownSymbol(iterator_key),
  toStringTag_wellKnownSymbol=retrieveWellKnownSymbol(toStringTag_key),
  isConcatSpreadable_wellKnownSymbol=retrieveWellKnownSymbol(isConcatSpreadable_key);

const IteratedObject_doubleBracketString="[[IteratedObject]]",
  ArrayIteratorNextIndex_doubleBracketString="[[ArrayIteratorNextIndex]]",
  ArrayIterationKind_doubleBracketString="[[ArrayIterationKind]]";

const IteratedObject_internalSlot=createUniqueSymbol(IteratedObject_doubleBracketString),
  ArrayIteratorNextIndex_internalSlot=createUniqueSymbol(ArrayIteratorNextIndex_doubleBracketString),
  ArrayIterationKind_internalSlot=createUniqueSymbol(ArrayIterationKind_doubleBracketString);

const key_itemKind="key",
  value_itemKind="value",
  key_plus_value_itemKind="key+value";

const ArrayIterator=(

temp0=function(array,kind){

returnOrThrowObjectType(array);
const self=returnOrThrowObjectType(this);
defineDontEnumProperty(
  self,
  IteratedObject_internalSlot,array
);
defineDontEnumProperty(
  self,
  ArrayIteratorNextIndex_internalSlot,0
);
defineDontEnumProperty(
  self,
  ArrayIterationKind_internalSlot,kind
);

  return self;
},

temp1=temp0[prototype_key],

tryES5DefineProperty(
  temp1,

  iterator_wellKnownSymbol,
  Object_valueOf,

  False,False,False
),

tryES5DefineProperty(
  temp1,

  "next",
  function(){

const self=returnOrThrowObjectType(this);
assertEager(
  (
    IteratedObject_internalSlot in self&&
    ArrayIteratorNextIndex_internalSlot in self&&
    ArrayIterationKind_internalSlot in self
  ),
  "Incompatible object: missing required properties!"
);
const a_var=self[IteratedObject_internalSlot];

    if(isPrimitive(a_var))return new IteratorResult(undef,True);

const index=self[ArrayIteratorNextIndex_internalSlot];
const itemKind=self[ArrayIterationKind_internalSlot];
const len=toLength(a_var[length_key]);
if(index>=len){

defineDontEnumProperty(
  self,
  IteratedObject_internalSlot,undef
);

    return new IteratorResult(undef,True);

}

defineDontEnumProperty(
  self,
  ArrayIteratorNextIndex_internalSlot,
  index+1
);

    if(itemKind===key_itemKind)return new IteratorResult(index,False);

const elementValue=a_var[index];
let result;
if(itemKind===value_itemKind)result=elementValue;
else{

if(itemKind!==key_plus_value_itemKind)throw(
  "Unsupported array iteration kind:\x20"+
  itemKind
);

(result=new Array_constructor(2))[0]=index;
result[1]=elementValue;

}

    return new IteratorResult(result,False);
  },

  False,False,False
),

tryES5DefineProperty(
  temp1,

  toStringTag_wellKnownSymbol,
  "Array Iterator",

  False,False,False
),

defineDontEnumProperty(
  temp1,
  IteratedObject_internalSlot,null
),

defineDontEnumProperty(
  temp1,
  ArrayIteratorNextIndex_internalSlot,0
),

defineDontEnumProperty(
  temp1,
  ArrayIterationKind_internalSlot,value_itemKind
),

  temp0
);

const arrayLike_entries_keys_values_maker=function(iterationKind){
  return function(){
    return new ArrayIterator(
      toObject(this),
      iterationKind
    );
  };
};

const arrayLikeEntries=arrayLike_entries_keys_values_maker(key_plus_value_itemKind),
  arrayLikeKeys=arrayLike_entries_keys_values_maker(key_itemKind),
  arrayLikeValues=arrayLike_entries_keys_values_maker(value_itemKind);

/* browser native item(s) */
/* Document Object Model (DOM) Level 1 Specification [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/ ] */

const bElement=returnOrThrowObjectType(bwindow.Element), /* http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-745549614 */
  bDocument=returnOrThrowObjectType(bwindow.Document), /* http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#i-Document */
  bNode=returnOrThrowObjectType(bwindow.Node); /* http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1950641247 */
const bElement_prototype=returnOrThrowObjectType(bElement[prototype_key]),
  bDocument_prototype=returnOrThrowObjectType(bDocument[prototype_key]),
  bNode_prototype=returnOrThrowObjectType(bNode[prototype_key]);

const returnOrThrowUint16=returnOrThrow_makeFunction(
  isUint16,
  function(value){
    return(
      "\x22"+
      value+
      "\x22 (type:\x20"+
      (value!==null?typeof value:value)+
      ") is not an unsigned 16-bit number!"
    );
  }
);

const ELEMENT_NODE_nodeType=returnOrThrowUint16(
  (temp0="ELEMENT_NODE") in (temp1=bNode_prototype)?temp1[temp0]:1
),
  ATTRIBUTE_NODE_nodeType=returnOrThrowUint16(
    (temp0="ATTRIBUTE_NODE") in temp1?temp1[temp0]:2
  ),
  TEXT_NODE_nodeType=returnOrThrowUint16(
    (temp0="TEXT_NODE") in temp1?temp1[temp0]:3
  ),
  CDATA_SECTION_NODE_nodeType=returnOrThrowUint16(
    (temp0="CDATA_SECTION_NODE") in temp1?temp1[temp0]:4
  ),
  PROCESSING_INSTRUCTION_NODE_nodeType=returnOrThrowUint16(
    (temp0="PROCESSING_INSTRUCTION_NODE") in temp1?temp1[temp0]:7
  ),
  COMMENT_NODE_nodeType=returnOrThrowUint16(
    (temp0="COMMENT_NODE") in temp1?temp1[temp0]:8
  ),
  DOCUMENT_NODE_nodeType=returnOrThrowUint16(
    (temp0="DOCUMENT_NODE") in temp1?temp1[temp0]:9
  ),
  DOCUMENT_FRAGMENT_NODE_nodeType=returnOrThrowUint16(
    (temp0="DOCUMENT_FRAGMENT_NODE") in temp1?temp1[temp0]:11
  );

const nodeType_key="nodeType",
  nodeName_key="nodeName",
  nodeValue_key="nodeValue";

const isDocument=function(value){

let key;

  return(
    value instanceof bDocument&&
    (key=nodeType_key) in value&&
    value[key]===DOCUMENT_NODE_nodeType&&
    (key=nodeName_key) in value&&
    value[key]==="#document"&&
    (key=nodeValue_key) in value&&
    value[key]===null
  );
};

const attributes_key="attributes";

const isElement=function(value){

let key;

  return(
    value instanceof bElement&&
    (key=nodeType_key) in value&&
    value[key]===ELEMENT_NODE_nodeType&&
    nodeName_key in value&&
    (key=nodeValue_key) in value&&
    value[key]===null&&
    (key=attributes_key) in value&&
    isArrayLike(value[key])
  );
};

const bdocument=returnOrThrowObjectType(bwindow.document);

const createTextNode=function(){ /* module: text node creation and manipulation */

const bDocument_createTextNode=returnOrThrowFunction(bDocument_prototype.createTextNode);

const bText=returnOrThrowObjectType(bwindow.Text); /* http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1312295772 */

const bTextIsFunction=isFunction(bText); /* false on Internet Explorer 9-11 */

const isTextNode=function(value){

let key,key0;

  return(
    value instanceof bText&&
    isArrayLike(value)&&
    (key=nodeType_key) in value&&
    value[key]===TEXT_NODE_nodeType&&
    (key=nodeName_key) in value&&
    value[key]==="#text"&&
    (key=nodeValue_key) in value&&
    (key0="data") in value&&
    value[key]===value[key0]
  );
};

function isNotATextNodeError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a text node ( http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1312295772 )!"
  );
}

const bText_prototype=returnOrThrowObjectType(bText[prototype_key]);

const bText_splitText=returnOrThrowFunction(bText_prototype.splitText);

const splitTextNode=function(textNode,offset){

  if(isTextNode(
    returnOrThrowObjectType(textNode)
  ))return Function_call[call_key](
    bText_splitText,
    textNode,
    offset
  );
  else throw isNotATextNodeError(textNode);
};

objectToExport.smartSplitTextNode=function(textNode,offset){
  return splitTextNode(
    returnOrThrowObjectType(textNode),
    roundInteger(0,textNode[length_key]-1,offset)
  );
};

objectToExport.isTextNode=isTextNode;
objectToExport.isNotATextNodeError=isNotATextNodeError;
objectToExport.splitTextNode=splitTextNode;

  return function(contents){
    return bTextIsFunction?new bText(contents):Function_call[call_key](bDocument_createTextNode,bdocument,contents);
  };
}();

const bElement_getAttribute=returnOrThrowFunction(bElement_prototype.getAttribute),
  bDocument_getElementsByTagName=returnOrThrowFunction(bDocument_prototype.getElementsByTagName);

(function(){ /* module: some static functions for element search and creation */

const getElementsByTagName=function(
  tagName, /* Supply string "*" to search all elements which are descendants of a document. */
  customDocument /* Supplying the second argument allows you to search elements in your given document. */
){
  return Function_call[call_key](
    bDocument_getElementsByTagName,
    isDocument(customDocument)?customDocument:bdocument, /* This is done by specifying the "this" value. */
    tagName
  );
};

objectToExport.getElementById=function(idName,customDocument){ /*
According to Document Object Model (HTML) Level 1 [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-html.html ],
"getElementById" [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-html.html#ID-36113835 ] is a method
implemented on interface "HTMLDocument" [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-html.html#ID-26809268 ]
instead of interface "Document" [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#i-Document ].
Therefore, this function generalizes the usage of method "getElementById"
by making it depending on method "getElementsByTagName" [ http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#method-getElementsByTagName ].
*/
const tempObject=getElementsByTagName("*",customDocument);
const indexFound=Function_call[call_key](
  arrayLikeFindIndex,
  tempObject,
  function(item){

let key;

    return(
      isElement(item)&&
      (
        Function_call[call_key](
          bElement_getAttribute,
          item,
          key="id"
        )===idName||
        (key in item&&item[key]===idName)
      )
    );
  }
);

  return(
    indexFound>-1&&
    isObjectType(tempObject)&&
    indexFound in tempObject
  )?tempObject[indexFound]:null;
};

objectToExport.getElementsByName=function(nameName,customDocument){
  return Function_call[call_key](
    arrayLikeFilter,
    getElementsByTagName("*",customDocument),
    function(item){

let key;

      return(
        isElement(item)&&
        (
          Function_call[call_key](
            bElement_getAttribute,
            item,
            key="name"
          )===nameName||
          (key in item&&item[key]===nameName)
        )
      );
    }
  );
};

objectToExport.getElementsByTagName=getElementsByTagName;

const bDocument_createDocumentFragment=returnOrThrowFunction(bDocument_prototype.createDocumentFragment),
  bDocumentFragment=returnOrThrowObjectType(bwindow.DocumentFragment); /* http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-B63ED1A3 */

objectToExport.isDocumentFragment=function(value){

let key;

  return(
    value instanceof bDocumentFragment&&
    (key=nodeType_key) in value&&
    value[key]===DOCUMENT_FRAGMENT_NODE_nodeType&&
    (key=nodeName_key) in value&&
    value[key]==="#document-fragment"&&
    (key=nodeValue_key) in value&&
    value[key]===null
  );
};

const bDocumentFragmentIsFunction=isFunction(bDocumentFragment);

objectToExport.createDocumentFragment=function(){
  return bDocumentFragmentIsFunction?new bDocumentFragment():Function_call[call_key](bDocument_createDocumentFragment,bdocument);
};

  return;
})();

const returnOrThrowElement=function(){

function isNotAnElementError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a DOM element ( http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-745549614 )!"
  );
}

objectToExport.isNotAnElementError=isNotAnElementError;

  return returnOrThrow_makeFunction(isElement,isNotAnElementError);
}();

const decorator_key="decorator";

const staticMethodDecorator0=(function decorator(actionFunction){

returnOrThrowFunction(actionFunction);

const functionToReturn=(function selfFn(target,keys,value){

let i_var,len;
if(isPrimitive(keys))actionFunction(target,keys,value);
else if(isArrayLike(keys)){ /* batch set, allowing you to set multiple attributes with one same value */

for(i_var=0,len=keys[length_key];i_var<len;i_var+=1)(
  Function_call[call_key](Object_hasOwnProperty,keys,i_var)&&
  selfFn(target,keys[i_var],value)
);

}else if(isPlainObject(keys)){ /* multiple set, allowing you to set different attributes with different values */

for(i_var in keys)(
  Function_call[call_key](Object_hasOwnProperty,keys,i_var)&&
  selfFn(
    target,i_var,
    keys[i_var] /*
Mention that the third argument will be ignored,
because the recursive call will override the third argument.
*/
  )
);

}else isFunction(keys)&&keys(target,value);

  return target;
});

functionToReturn[decorator_key]=decorator; /*
This decorator function is so useful that
we feel guilty about keeping this marvelous tool to just ourselves.
*/

  return functionToReturn;
});

const Array_push=returnOrThrowFunction(Array_prototype.push);

const instanceMethodDecorator0_DOMOperation=function(){

const helperFunction=function(firstArgument,laterArguments){

const argumentsToSupply=new Array_constructor();
Function_call[call_key](Array_push,argumentsToSupply,firstArgument);
Function_call[call_key](Function_apply,Array_push,argumentsToSupply,laterArguments);

  return argumentsToSupply;
};

  return function decorator(checkFunction,operationFunction){

returnOrThrowFunction(checkFunction);
returnOrThrowFunction(operationFunction);

const functionToReturn=function(){

const self=this;
let i_var,len,j_var,args;

      if(isArrayLike(self)){

args=arguments;
for(i_var=0,len=self[length_key];i_var<len;i_var+=1)(
  Function_call[call_key](Object_hasOwnProperty,self,i_var)&&
  (
    checkFunction(j_var=self[i_var])&&
    Function_call[call_key](
      Function_apply,
      operationFunction,
      null,
      helperFunction(j_var,args)
    )
  )
);

}

      return self;
};

functionToReturn[decorator_key]=decorator;

    return functionToReturn;
  };
}();

const reference_key="reference",
  isArrayLike_key="isArrayLike";

const reference_uniqueSymbol=createUniqueSymbol(reference_key),
  isArrayLike_uniqueSymbol=createUniqueSymbol(isArrayLike_key);

assertEager(
  (temp0=iterator_wellKnownSymbol)!==toStringTag_wellKnownSymbol&&
  temp0!==isConcatSpreadable_wellKnownSymbol&&
  temp0!==IteratedObject_internalSlot&&
  temp0!==ArrayIteratorNextIndex_internalSlot&&
  temp0!==ArrayIterationKind_internalSlot&&
  temp0!==reference_uniqueSymbol&&
  temp0!==isArrayLike_uniqueSymbol
); /* new action: checks whether there are duplicate keys */

const DOMOperation=function(){

function constructorRequireNewError(value){
  return(
    "\x22"+
    value+
    "\x22 constructor: \x27new\x27 is required!"
  );
}

  return function constructorFn(target,dontRefer){

const self=this;

    if(!(self instanceof constructorFn))throw constructorRequireNewError(constructorFn);

let i_var,len;

if(isNotNullish(target)){

if(isArrayLike(target)){

len=target[length_key];
defineDontEnumProperty(self,length_key,len);
for(i_var=0;i_var<len;i_var+=1)(
  Function_call[call_key](Object_hasOwnProperty,target,i_var)&&
  (self[i_var]=target[i_var])
);
defineDontEnumProperty(self,isArrayLike_uniqueSymbol,True);
(
  dontRefer||
  isArray(target)||
  target instanceof constructorFn||
  defineDontEnumProperty(self,reference_uniqueSymbol,target)
);

}else{

defineDontEnumProperty(self,length_key,1);
self[0]=target;

}

}

    return self;
  };
}();

const DOMOperation_prototype=tryES5DefineProperty(
  DOMOperation[prototype_key],

  length_key,0,

  True,False,False
);

assignMultipleProperties(
  DOMOperation_prototype,

  createNullPrototypeArrayLikeObject(
    reference_uniqueSymbol,
    isArrayLike_uniqueSymbol
  ),
  createNullPrototypeArrayLikeObject(
    null,
    False
  ),

  True,False,True
);

const bElement_setAttribute=returnOrThrowFunction(bElement_prototype.setAttribute);

(function(){ /* module: attributes and properties manipulation */

objectToExport.attributesGetter=function(target,names){

returnOrThrowElement(
  returnOrThrowObjectType(target)
);

  if(isNullish(names))return target[attributes_key];
  else if(isPrimitive(names))return Function_call[call_key]( /* single get */
    bElement_getAttribute,
    target,
    names
  );
  else if(isArrayLike(names))return Function_call[call_key]( /* ordered multiple get */
    arrayLikeMap,
    names,
    bElement_getAttribute,
    target
  );
  else if(isFunction(names))return names(target); /* allows customized operations */
  else return null; /* default value */
};

const attributesSetter=staticMethodDecorator0(function(){

const bElement_removeAttribute=returnOrThrowFunction(bElement_prototype.removeAttribute);

  return function(targetElement,attributeName,attributeValue){

returnOrThrowElement(
  returnOrThrowObjectType(targetElement)
);
Function_call[call_key](
  isNotNullish(attributeValue)?bElement_setAttribute:bElement_removeAttribute, /* Supplying the third argument with null or undefined allows you to remove that attribute. */
  targetElement,
  attributeName,
  isFunction(attributeValue)?attributeValue(targetElement,attributeName):attributeValue /*
If the callback function returns nothing,
string "undefined" will be set,
because at this point,
the function will not recursively call itself any further.
*/
);

    return;
  };
}());

const smartSetDOMProperties=staticMethodDecorator0(function(target,key,values){

let t_var,i_var;
if(isPrimitive(t_var=returnOrThrowObjectType(target)[key]))target[key]=values;
else{

if(isObjectType(values)){ /* The object-merging mechanism keeps the target object properties intact. */

if(isPlainObject(values)){

for(i_var in values)(
  Function_call[call_key](Object_hasOwnProperty,values,i_var)&&
  (t_var[i_var]=values[i_var])
);

}else isFunction(values)&&values(t_var,key,target);

}else target[key]=values;

}

  return;
});

objectToExport.createElement=function(){

const bDocument_createElement=returnOrThrowFunction(bDocument_prototype.createElement);

  return function(name){

const newElement=isElement(name)?name:Function_call[call_key](bDocument_createElement,bdocument,name);
const args=arguments;
let i_var,len,j_var;
for(i_var=1,len=args[length_key];i_var<len;i_var+=1)(
  isObjectType(j_var=args[i_var])&&
  (i_var&1?smartSetDOMProperties:attributesSetter)(newElement,j_var)
);

    return newElement;
  };
}();

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="attributesSetter",
  instanceMethodDecorator0_DOMOperation(isElement,attributesSetter)
);

objectToExport[temp0]=attributesSetter;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="smartSetDOMProperties",
  instanceMethodDecorator0_DOMOperation(isObjectType,smartSetDOMProperties)
);

objectToExport[temp0]=smartSetDOMProperties;

  return;
})();

const isNode=function(){

const isValidNodeType=function(){

const ENTITY_REFERENCE_NODE_nodeType=returnOrThrowUint16(
  (temp0="ENTITY_REFERENCE_NODE") in (temp1=bNode_prototype)?temp1[temp0]:5
),
  ENTITY_NODE_nodeType=returnOrThrowUint16(
    (temp0="ENTITY_NODE") in temp1?temp1[temp0]:6
  ),
  DOCUMENT_TYPE_NODE_nodeType=returnOrThrowUint16(
    (temp0="DOCUMENT_TYPE_NODE") in temp1?temp1[temp0]:10
  ),
  NOTATION_NODE_nodeType=returnOrThrowUint16(
    (temp0="NOTATION_NODE") in temp1?temp1[temp0]:12
  );

temp0=function(num){

switch(num){
  case ELEMENT_NODE_nodeType:
  case ATTRIBUTE_NODE_nodeType:
  case TEXT_NODE_nodeType:
  case CDATA_SECTION_NODE_nodeType:
  case ENTITY_REFERENCE_NODE_nodeType:
  case ENTITY_NODE_nodeType:
  case PROCESSING_INSTRUCTION_NODE_nodeType:
  case COMMENT_NODE_nodeType:
  case DOCUMENT_NODE_nodeType:
  case DOCUMENT_TYPE_NODE_nodeType:
  case DOCUMENT_FRAGMENT_NODE_nodeType:
  case NOTATION_NODE_nodeType:

    return True;

}

  return False;
};

temp0.validValues=Function_call[call_key](
  Function_apply,
  createNullPrototypeArrayLikeObject,
  null,
  [
    ELEMENT_NODE_nodeType,
    ATTRIBUTE_NODE_nodeType,
    TEXT_NODE_nodeType,
    CDATA_SECTION_NODE_nodeType,
    ENTITY_REFERENCE_NODE_nodeType,
    ENTITY_NODE_nodeType,
    PROCESSING_INSTRUCTION_NODE_nodeType,
    COMMENT_NODE_nodeType,
    DOCUMENT_NODE_nodeType,
    DOCUMENT_TYPE_NODE_nodeType,
    DOCUMENT_FRAGMENT_NODE_nodeType,
    NOTATION_NODE_nodeType
  ]
);

  return temp0;
}();

objectToExport.isValidNodeType=isValidNodeType;

  return function(value){

let key;

    return(
      value instanceof bNode&&
      (key=nodeType_key) in value&&
      isValidNodeType(value[key])&&
      nodeName_key in value&&
      nodeValue_key in value
    );
  };
}();

const returnOrThrowNode=function(){

function isNotANodeError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a DOM node ( http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1950641247 )!"
  );
}

objectToExport.isNotANodeError=isNotANodeError;

  return returnOrThrow_makeFunction(isNode,isNotANodeError);
}();

const instanceMethodDecorator1_DOMOperation=(function decorator(arrayLikeFilter_callbackFn,arrayLikeForEach_callbackFn){

returnOrThrowFunction(arrayLikeFilter_callbackFn);
returnOrThrowFunction(arrayLikeForEach_callbackFn);

const functionToReturn=function(){

const self=this;
isObjectType(self)&&Function_call[call_key](
  arrayLikeForEach,
  Function_call[call_key](
    arrayLikeFilter,
    self,
    arrayLikeFilter_callbackFn
  ),
  arrayLikeForEach_callbackFn
);

  return self;
};

functionToReturn[decorator_key]=decorator;

  return functionToReturn;
});

const bNode_appendChild=returnOrThrowFunction(bNode_prototype.appendChild);

(function(){ /* module: node children and node inner text manipulation */

const bNode_removeChild=returnOrThrowFunction(bNode_prototype.removeChild);

const removeAllChildren=function(target){

returnOrThrowNode(
  returnOrThrowObjectType(target)
);

let tempNode;
while(isNode(tempNode=target.firstChild))
  Function_call[call_key](bNode_removeChild,target,tempNode);

  return target;
};

const safeModifyNodeText=(function selfFn(target,text){

let target_nodeType;

switch(target_nodeType=returnOrThrowNode(
  returnOrThrowObjectType(target)
)[nodeType_key]){

  case TEXT_NODE_nodeType:
  case CDATA_SECTION_NODE_nodeType:
  case COMMENT_NODE_nodeType:
  case ATTRIBUTE_NODE_nodeType:
  case PROCESSING_INSTRUCTION_NODE_nodeType:

target[nodeValue_key]=isFunction(text)?text(target,target_nodeType):text; /* allows callback functions */

    break;
  case ELEMENT_NODE_nodeType:
  case DOCUMENT_FRAGMENT_NODE_nodeType:

removeAllChildren(target);
Function_call[call_key](
  bNode_appendChild,
  target,
  createTextNode(
    isFunction(text)?text(target,target_nodeType):text
  )
);

    break;
  case DOCUMENT_NODE_nodeType:
    selfFn(target.documentElement,text);
    break;
  default:
    throw(
      "Unsupported type:\x20"+
      target+
      "\n("+
      nodeType_key+
      ":\x20"+
      target_nodeType+
      ")"
    );
}

  return target;
});

const parentNode_key="parentNode";

const removeSelfFromParentNode=function(target){

let parentNode;
(
  isNode(
    parentNode=returnOrThrowNode(
      returnOrThrowObjectType(target)
    )[parentNode_key]
  )&&
  Function_call[call_key](bNode_removeChild,parentNode,target)
);

  return target;
};

objectToExport.replaceNode=function(){

const bNode_replaceChild=returnOrThrowFunction(bNode_prototype.replaceChild);

  return function(nodeToReplace,nodeToBeReplaced){

returnOrThrowNode(
  returnOrThrowObjectType(nodeToReplace)
);
let nodeToBeReplaced_parentNode;
(
  isNode(nodeToBeReplaced)&&
  isNode(nodeToBeReplaced_parentNode=nodeToBeReplaced[parentNode_key])&&
  Function_call[call_key](
    bNode_replaceChild,
    nodeToBeReplaced_parentNode,
    nodeToReplace,
    nodeToBeReplaced
  )
);

    return nodeToReplace;
  };
}();

const nextSibling_key="nextSibling";

(function(){

const bNode_insertBefore=returnOrThrowFunction(bNode_prototype.insertBefore);

objectToExport.insertNodeBefore=function(nodeToInsert,referenceNode){

returnOrThrowNode(
  returnOrThrowObjectType(nodeToInsert)
);
let referenceNode_parentNode;
(
  isNode(referenceNode)&&
  isNode(referenceNode_parentNode=referenceNode[parentNode_key])&&
  Function_call[call_key](
    bNode_insertBefore,
    referenceNode_parentNode,
    nodeToInsert,
    referenceNode
  )
);

  return nodeToInsert;
};

objectToExport.insertNodeAfter=function(nodeToInsert,referenceNode){

returnOrThrowNode(
  returnOrThrowObjectType(nodeToInsert)
);
let referenceNode_parentNode,referenceNode_nextSibling;
(
  isNode(referenceNode)&&
  isNode(referenceNode_parentNode=referenceNode[parentNode_key])&&(
    (
      referenceNode_parentNode.lastChild!==referenceNode&&
      isNode(referenceNode_nextSibling=referenceNode[nextSibling_key])
    )?Function_call[call_key](
      bNode_insertBefore,
      referenceNode_parentNode,
      nodeToInsert,
      referenceNode_nextSibling
    ):Function_call[call_key](
      bNode_appendChild,
      referenceNode_parentNode,
      nodeToInsert
    )
  )
);

  return nodeToInsert;
};

  return;
})();

objectToExport.cloneNode=function(){

const bNode_cloneNode=returnOrThrowFunction(bNode_prototype.cloneNode);

  return function(nodeToClone,deepClone){
    return Function_call[call_key](
      bNode_cloneNode,
      returnOrThrowNode(
        returnOrThrowObjectType(nodeToClone)
      ),
      deepClone?True:False
    );
  };
}();

const removeSiblings=function(targetNode,spareLeft,spareRight){

let parentNode,siblingNode;

if(isNode(parentNode=returnOrThrowNode(
  returnOrThrowObjectType(targetNode)
)[parentNode_key])){

if(!spareLeft){

while(isNode(siblingNode=targetNode.previousSibling))
  Function_call[call_key](bNode_removeChild,parentNode,siblingNode);

}

if(!spareRight){

while(isNode(siblingNode=targetNode[nextSibling_key]))
  Function_call[call_key](bNode_removeChild,parentNode,siblingNode);

}

}

  return targetNode;
};

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="removeAllChildren",
  instanceMethodDecorator1_DOMOperation(isNode,removeAllChildren)
);

objectToExport[temp0]=removeAllChildren;

temp0=instanceMethodDecorator0_DOMOperation(
  function(item){

    if(isNode(item)){

switch(item[nodeType_key]){
  case TEXT_NODE_nodeType:
  case CDATA_SECTION_NODE_nodeType:
  case COMMENT_NODE_nodeType:
  case ATTRIBUTE_NODE_nodeType:
  case PROCESSING_INSTRUCTION_NODE_nodeType:
  case ELEMENT_NODE_nodeType:
  case DOCUMENT_FRAGMENT_NODE_nodeType:
  case DOCUMENT_NODE_nodeType:
        return True;
  default:
        return False;
}

    }else return False;
  },
  safeModifyNodeText
);

temp1=function(){
  return Function_call[call_key](
    Function_apply,
    createNullPrototypeArrayLikeObject,
    null,
    [
      TEXT_NODE_nodeType,
      CDATA_SECTION_NODE_nodeType,
      COMMENT_NODE_nodeType,
      ATTRIBUTE_NODE_nodeType,
      PROCESSING_INSTRUCTION_NODE_nodeType,
      ELEMENT_NODE_nodeType,
      DOCUMENT_FRAGMENT_NODE_nodeType,
      DOCUMENT_NODE_nodeType
    ]
  );
};

temp0[temp2="validValues"]=temp1();
safeModifyNodeText[temp2]=temp1();

defineDontEnumProperty(
  DOMOperation_prototype,
  temp2="safeModifyNodeText",
  temp0
);

objectToExport[temp2]=safeModifyNodeText;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="removeSelfFromParentNode",
  instanceMethodDecorator1_DOMOperation(isNode,removeSelfFromParentNode)
);

objectToExport[temp0]=removeSelfFromParentNode;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="removeSiblings",
  instanceMethodDecorator0_DOMOperation(isNode,removeSiblings)
);

objectToExport[temp0]=removeSiblings;

  return;
})();

(function(){ /* module: events manipulation */

const smartAddEventListener=staticMethodDecorator0(function(target,eventName,eventFn){
  if(
    isPrimitive(target)||
    !isFunction(eventFn)
  )return;

let eventName_on,originalEvent;
if((eventName_on="on"+eventName) in target){

originalEvent=target[eventName_on];
target[eventName_on]=isFunction(originalEvent)?function(){

const newEvent=eventFn;

    return function(){

const self=this,
  args=arguments;
let originalEvent_returnValue,newEvent_returnValue; /*
Return values from callback functions are important,
because "return false;" will have almost equivalent effects as "event.preventDefault()" [ http://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault ]
in major and modern browsers.
*/
try{

originalEvent_returnValue=Function_call[call_key](Function_apply,originalEvent,self,args);

}finally{

newEvent_returnValue=Function_call[call_key](Function_apply,newEvent,self,args);

}

      return originalEvent_returnValue||newEvent_returnValue;
    };
}():eventFn;

}

  return;
});

const smartPreventDefault=function(){

const smartPreventDefaultEvent=function(eventObject){

let tempKey;
if(isObjectType(eventObject)){

(
  (tempKey="cancelable") in eventObject&&
  eventObject[tempKey]&& /* prerequisite */
  (tempKey="defaultPrevented") in eventObject&&
  !eventObject[tempKey]&& /* necessity */
  (tempKey="preventDefault") in eventObject&&
  isFunction(tempKey=eventObject[tempKey])&&
  Function_call[call_key](tempKey,eventObject) /* action */
);
(tempKey="returnValue") in eventObject&&(eventObject[tempKey]=False); /* legacy support */

}

  return False; /*
This return value makes this function eligible for
being attached to a property of an object
or passed in as the second argument of "addEventListener()" method [ http://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener ].
*/
};

objectToExport.smartPreventDefaultEvent=smartPreventDefaultEvent;

  return function(target,eventNames){
    return smartAddEventListener(target,eventNames,smartPreventDefaultEvent);
  };
}();

objectToExport.smartStopPropagation=function(eventObject){

let tempKey;
if(isObjectType(eventObject)){

(
  (tempKey="stopPropagation") in eventObject&&
  isFunction(tempKey=eventObject[tempKey])&&
  Function_call[call_key](tempKey,eventObject)
);
(tempKey="cancelBubble") in eventObject&&(eventObject[tempKey]=True); /* legacy support */

}

  return eventObject;
};

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="smartAddEventListener",
  instanceMethodDecorator0_DOMOperation(isObjectType,smartAddEventListener)
);

objectToExport[temp0]=smartAddEventListener;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="smartPreventDefault",
  instanceMethodDecorator0_DOMOperation(isObjectType,smartPreventDefault)
);

objectToExport[temp0]=smartPreventDefault;

  return;
})();

const Array_slice=returnOrThrowFunction(Array_prototype.slice);

(function(){ /* module: element classes manipulation */

const isRegExp=function(value){
  return isObjectType(value)&&value[constructor_key]===RegExp_constructor;
};

const segmentStringWithDelimiter=function(){

const String_split=returnOrThrowFunction(String_prototype.split);

  return function(originalString,delimiter,needFiltering){ /*
This function returns an array.
It can be regarded as an enhanced version of "String.prototype.split".
*/

let resultArray;

if(isNotNullish(originalString)){

resultArray=Function_call[call_key](
  String_split,
  originalString,
  isRegExp(delimiter)?new RegExp_constructor(delimiter):delimiter /* Create a new RegExp object to prevent property changes on the original RegExp object. */
);
needFiltering&&(resultArray=Function_call[call_key](
  arrayLikeFilter,
  resultArray,
  identity
));

}else resultArray=new Array_constructor();

    return resultArray;
  };
}();

const Array_join=returnOrThrowFunction(Array_prototype.join);

const concatStringWithDelimiter=function(){

const Array_concat=returnOrThrowFunction(Array_prototype.concat);

  return function(originalString,delimiter,newContents,needFiltering){

let result,key;

if(
  isNullish(originalString)||
  isNullish(newContents)
)result="";
else{

result=segmentStringWithDelimiter(originalString,delimiter,needFiltering);
if(isPrimitive(newContents))result=Function_call[call_key](Array_concat,result,newContents);
else if(isArrayLike(newContents))result=Function_call[call_key](
  Function_apply,
  Array_concat,
  result,
  Function_call[call_key](Array_slice,newContents) /* performs a conversion from array-like objects to arrays */
);
else if(isFunction(newContents))result=Function_call[call_key](arrayLikeMap,result,newContents);
else if(isPlainObject(newContents)){

for(key in newContents)(
  Function_call[call_key](Object_hasOwnProperty,newContents,key)&&
  Function_call[call_key](Array_push,result,newContents[key])
);

}

result=Function_call[call_key](
  Array_join,
  result,
  isRegExp(delimiter)?delimiter.source:delimiter
);

}

    return result;
  };
}();

const class_attributeName="class",
  className_key="className";

const smartGetClasses=function(target){
  return arrayLikeDeduplicate(
    segmentStringWithDelimiter(
      isElement(
        returnOrThrowObjectType(target)
      )?Function_call[call_key](
        bElement_getAttribute,
        target,
        class_attributeName
      ):target[className_key],
      new RegExp_constructor("\\s+"),
      True
    )
  );
};

const hasClass=function(target,name){

returnOrThrowObjectType(target);

  if(isPrimitive(name))return Function_call[call_key](
    arrayLikeIndexOf,
    smartGetClasses(target),
    name
  )>-1;
  else return(
    isFunction(name)&&
    Function_call[call_key](
      arrayLikeSome,
      smartGetClasses(target),
      name
    )
  )?True:False;
};

objectToExport.getElementsByClassName=function(className,customDocument){
  return Function_call[call_key](
    arrayLikeFilter,
    Function_call[call_key](
      bDocument_getElementsByTagName,
      isDocument(customDocument)?customDocument:bdocument,
      "*"
    ),
    function(item){
      return(
        isObjectType(item)&&
        hasClass(item,className)
      );
    }
  );
};

objectToExport.hasClass=hasClass;

const staticMethodDecorator1=(function decorator(callbackFunction){

returnOrThrowFunction(callbackFunction);

const functionToReturn=function(arg0,arg1){
  return callbackFunction(
    arg0,
    isArrayLike(arg1)?arg1:Function_call[call_key](
      Array_slice,arguments,1 /* This is a costly operation, so it must be lazy evaluated. */
    )
  ); /* As you can see, the callback function takes exactly 2 arguments, with the second argument guaranteed to be an array-like object. */
};

functionToReturn[decorator_key]=decorator;

  return functionToReturn;
});

const setClassAttribute=function(
  target,
  value /* This function performs no checks on this argument. */
){

isElement(
  returnOrThrowObjectType(target)
)?Function_call[call_key](
  bElement_setAttribute,
  target,
  class_attributeName,
  value
):(target[className_key]=value);

  return target;
};

const smartAddClasses=staticMethodDecorator1(function(){

const oneSpace_string="\x20";

  return function(target,classes){
    return setClassAttribute(
      returnOrThrowObjectType(target),
      concatStringWithDelimiter(
        Function_call[call_key](
          Array_join,
          smartGetClasses(target),
          oneSpace_string
        ),
        oneSpace_string,
        classes,
        True
      )
    );
  };
}());

const removeAllClasses=function(target){
  return setClassAttribute(target,"");
};

const smartRemoveClasses=staticMethodDecorator1(function(){

const removeOneClass=function(){

const Array_splice=returnOrThrowFunction(Array_prototype.splice);

  return function(target,className){

const classNames=smartGetClasses(
  returnOrThrowObjectType(target)
);
let indexFound;
if(isPrimitive(className))indexFound=Function_call[call_key](
  arrayLikeIndexOf,
  classNames,
  className
);else if(isFunction(className))indexFound=Function_call[call_key](
  arrayLikeFindIndex,
  classNames,
  className
);
if(
  isNotNullish(indexFound)&&
  indexFound>-1
){
  Function_call[call_key](Array_splice,classNames,indexFound,1);
  removeAllClasses(target);
  smartAddClasses(target,classNames);
}

    return target;
  };
}();

  return function(target,classes){

returnOrThrowObjectType(target);
let i_var,len;
for(i_var=0,len=classes[length_key];i_var<len;i_var+=1)(
  Function_call[call_key](Object_hasOwnProperty,classes,i_var)&&
  removeOneClass(target,classes[i_var])
);

    return target;
  };
}());

objectToExport.segmentStringWithDelimiter=segmentStringWithDelimiter;
objectToExport.concatStringWithDelimiter=concatStringWithDelimiter;
objectToExport.smartGetClasses=smartGetClasses;

const instanceMethodDecorator2_DOMOperation=(function decorator(checkFunction,operationFunction){

returnOrThrowFunction(checkFunction);
returnOrThrowFunction(operationFunction);

const functionToReturn=function(arg0){

const self=this;
let arrayLikeObject,i_var,len,j_var;
if(isArrayLike(self)){

arrayLikeObject=isArrayLike(arg0)?arg0:arguments;
for(i_var=0,len=self[length_key];i_var<len;i_var+=1)(
  Function_call[call_key](Object_hasOwnProperty,self,i_var)&&
  (
    checkFunction(j_var=self[i_var])&&
    operationFunction(j_var,arrayLikeObject)
  )
);

}

  return self;
};

functionToReturn[decorator_key]=decorator;

  return functionToReturn;
});

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="smartAddClasses",
  instanceMethodDecorator2_DOMOperation(isObjectType,smartAddClasses)
);

objectToExport[temp0]=smartAddClasses;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="removeAllClasses",
  instanceMethodDecorator1_DOMOperation(isObjectType,removeAllClasses)
);

objectToExport[temp0]=removeAllClasses;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="smartRemoveClasses",
  instanceMethodDecorator2_DOMOperation(isObjectType,smartRemoveClasses)
);

objectToExport[temp0]=smartRemoveClasses;

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="setClassAttribute",
  instanceMethodDecorator0_DOMOperation(isObjectType,setClassAttribute)
);

objectToExport[temp0]=setClassAttribute;

  return;
})();

(function(){ /* module: element style manipulation */

const changeElementStyle=function(){

const helperFunction=staticMethodDecorator0(function(target,key,value){

(
  key in returnOrThrowObjectType(target)&&
  (target[key]=value)
);

  return;
});

  return function(target,keys,values){

let temp;
(
  (temp="style") in returnOrThrowObjectType(target)&&
  isObjectType(temp=target[temp])&&
  helperFunction(temp,keys,values)
);

    return target;
  };
}();

defineDontEnumProperty(
  DOMOperation_prototype,
  temp0="changeElementStyle",
  instanceMethodDecorator0_DOMOperation(isObjectType,changeElementStyle)
);

objectToExport[temp0]=changeElementStyle;

  return;
})();

(function(){ /* module: append multiple node children to a single node */

objectToExport.appendChildren=(function selfFn(target,singleNodeOrNodeArray){

returnOrThrowNode(
  returnOrThrowObjectType(target)
);

let returnValue;

switch(True){
  case isNode(singleNodeOrNodeArray): /*
There are too many DOM node objects containing a "length" property
which can be easily regarded as array-like objects,
hence this must be checked before other operations.
*/

returnValue=selfFn(
  target,
  Function_call[call_key](Array_slice,arguments,1)
);

    break;

  case isArrayLike(singleNodeOrNodeArray):

Function_call[call_key](
  arrayLikeEvery,
  Function_call[call_key](
    arrayLikeFilter,
    singleNodeOrNodeArray,
    isNode
  ),
  bNode_appendChild,
  target
);

  default: /* eslint-disable-line no-fallthrough */

returnValue=target;

}

  return returnValue;
});

  return;
})();

defineDontEnumProperty(
  DOMOperation_prototype,
  "toArray",
  function(){
    return Function_call[call_key](Array_slice,this);
  }
);

defineDontEnumProperty(
  DOMOperation_prototype,
  valueOf_key,
  function(){
    const self=this;
    return forceToInteger(
      isObjectType(self)?self[length_key]:self
    );
  }
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "findIndex",arrayLikeFindIndex
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "filter",arrayLikeFilter
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "map",arrayLikeMap
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "forEach",arrayLikeForEach
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "indexOf",arrayLikeIndexOf
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "every",arrayLikeEvery
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "some",arrayLikeSome
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "entries",arrayLikeEntries
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "keys",arrayLikeKeys
);

defineDontEnumProperty(
  DOMOperation_prototype,
  "values",arrayLikeValues
);

defineDontEnumProperty(
  DOMOperation_prototype,
  iterator_wellKnownSymbol,arrayLikeValues
);

defineDontEnumProperty(
  DOMOperation_prototype,
  isConcatSpreadable_wellKnownSymbol,True
);

objectToExport.isDocument=isDocument;
objectToExport.isElement=isElement;
objectToExport.createTextNode=createTextNode;
objectToExport.isNode=isNode;

objectToExport.DOMOperation=DOMOperation;
objectToExport.IteratorResult=IteratorResult;
objectToExport.ArrayIterator=ArrayIterator;

isUint32[temp0="MAX_VALUE"]=UNSIGNED_INT32_MAX;
isInteger[temp0]=SIGNED_INT32_MAX;
toLength[temp0]=mathPow2_53minus1;
isUint16[temp0]=UNSIGNED_INT16_MAX;
isInteger.MIN_VALUE=SIGNED_INT32_MIN;

(temp0=tryES5CreateNullPrototypeObject())[iterator_key]=iterator_wellKnownSymbol;
temp0[toStringTag_key]=toStringTag_wellKnownSymbol;
temp0[isConcatSpreadable_key]=isConcatSpreadable_wellKnownSymbol;

objectToExport.wellKnownSymbols=temp0;

(temp0=tryES5CreateNullPrototypeObject())[IteratedObject_doubleBracketString]=IteratedObject_internalSlot;
temp0[ArrayIteratorNextIndex_doubleBracketString]=ArrayIteratorNextIndex_internalSlot;
temp0[ArrayIterationKind_doubleBracketString]=ArrayIterationKind_internalSlot;
temp0[reference_key]=reference_uniqueSymbol;
temp0[isArrayLike_key]=isArrayLike_uniqueSymbol;

objectToExport.uniqueSymbols=temp0;

(temp0=tryES5CreateNullPrototypeObject())[key_itemKind]=key_itemKind;
temp0[value_itemKind]=value_itemKind;
temp0[key_plus_value_itemKind]=key_plus_value_itemKind;

ArrayIterator.allowedKinds=temp0;

objectToExport.isUint32=isUint32;
objectToExport.isUint16=isUint16;
objectToExport.isInteger=isInteger;
objectToExport.isPlainObject=isPlainObject;
objectToExport.isArrayLike=isArrayLike;
objectToExport.roundInteger=roundInteger;
objectToExport.forceToInteger=forceToInteger;
objectToExport.isFiniteNumber=isFiniteNumber;
objectToExport.isNotInfinity=isNotInfinity;
objectToExport.arrayLikeFindIndex=arrayLikeFindIndex;
objectToExport.toLength=toLength;
objectToExport.arrayLikeFilter=arrayLikeFilter;
objectToExport.toUint32=toUint32;
objectToExport.arrayLikeMap=arrayLikeMap;
objectToExport.arrayLikeForEach=arrayLikeForEach;
objectToExport.arrayLikeIndexOf=arrayLikeIndexOf;
objectToExport.arrayLikeEvery=arrayLikeEvery;
objectToExport.arrayLikeSome=arrayLikeSome;
objectToExport.arrayLikeDeduplicate=arrayLikeDeduplicate;
objectToExport.arrayLikeEntries=arrayLikeEntries;
objectToExport.arrayLikeKeys=arrayLikeKeys;
objectToExport.arrayLikeValues=arrayLikeValues;

objectToExport.createNullPrototypeArrayLikeObject=createNullPrototypeArrayLikeObject;
objectToExport.assignMultipleProperties=assignMultipleProperties;

assignMultipleProperties(
  objectToExport,

  createNullPrototypeArrayLikeObject(
    "author",
    "date"
  ),
  createNullPrototypeArrayLikeObject(
    "Bright_Leader",
    1738737305040
  ),

  True,False,True
);

    return objectToExport;
  }
);